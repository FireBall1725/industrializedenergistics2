package com.fireball1725.ae2tech.container.machines;

import com.fireball1725.ae2tech.container.BaseContainer;
import com.fireball1725.ae2tech.container.slot.SlotRestrictedInput;
import com.fireball1725.ae2tech.tileentity.machines.TileEntitySyncPlate;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
/*	Created by Penagwin
 *	7/22/14
 * 	
 */
public class ContainerSyncPlate extends BaseContainer {
    public int progress = -1;
    public int upgradeTier = 0;
    public IInventory inventory;
    public TileEntitySyncPlate tileEntitySyncPlate;

    public ContainerSyncPlate(InventoryPlayer inventoryPlayer, TileEntity tileEntity) {
        super(inventoryPlayer, tileEntity);
        this.inventory = (IInventory) tileEntity;
        this.tileEntitySyncPlate = (TileEntitySyncPlate) tileEntity;
        // Upgrade Slot #1 & #2
        addSlotToContainer(new SlotRestrictedInput(SlotRestrictedInput.PlaceableItemType.UPGRADES, inventory, 36, 187, 8).setStackLimit(1));
        addSlotToContainer(new SlotRestrictedInput(SlotRestrictedInput.PlaceableItemType.UPGRADES, inventory, 37, 187, 8 + 18).setStackLimit(1));

        // Bind Player Slots
        bindInventory(inventory, 0,28);
        bindPlayerInventory(inventoryPlayer, 0, 122);
    }

    protected int getHeight() {
        return 152;
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i) {
            @SuppressWarnings("unused")
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

        }

        this.upgradeTier = this.tileEntitySyncPlate.getUpgradeTier();
    }

    public void setSyncIO(Boolean status){
    	tileEntitySyncPlate.IO = status;
    }
    public boolean getSyncIO(){
    	return tileEntitySyncPlate.IO;
    }
    
}
