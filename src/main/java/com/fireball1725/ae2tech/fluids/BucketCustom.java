package com.fireball1725.ae2tech.fluids;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.FillBucketEvent;

/**
 * Created by Penagwin on 8/19/2014.
 */
public class BucketCustom extends ItemBucket{
    public BucketCustom(Block p_i45331_1_) {
        super(p_i45331_1_);
    }
}
