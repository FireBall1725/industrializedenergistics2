package com.fireball1725.ae2tech.core.sync;

import com.fireball1725.ae2tech.core.sync.packets.PacketInventoryAction;
import com.ibm.icu.impl.IllegalIcuArgumentException;

import io.netty.buffer.ByteBuf;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;

public class NetworkPacketHandlerBase {
    public static Map<Class, PacketTypes> reverseLookup = new HashMap();

    public static enum PacketTypes {
        PACKET_INVENTORY_ACTION(PacketInventoryAction.class),;

        public final Class pc;
        public final Constructor con;

        private PacketTypes(Class c) {
            this.pc = c;

            Constructor x = null;
            try {
                x = this.pc.getConstructor(new Class[] {ByteBuf.class});
            } catch (NoSuchMethodException e) {} catch (SecurityException e) {}
            this.con = x;
            NetworkPacketHandlerBase.reverseLookup.put(this.pc, this);
            if (this.con == null) {
                throw new RuntimeException("Invalid Packet Class...");
            }
        }

        public NetworkPacket parsePacket(ByteBuf in) throws InstantiationException, IllegalAccessException, IllegalIcuArgumentException, InvocationTargetException {
            return (NetworkPacket)this.con.newInstance(new Object[] { in });
        }

        public static PacketTypes getPacket(int id) {
            return values()[id];
        }

        public static PacketTypes getID(Class<? extends NetworkPacket> c) {
            return (PacketTypes)NetworkPacketHandlerBase.reverseLookup.get(c);
        }
    }
}
