package com.fireball1725.ae2tech.blocks.machines;

import java.util.Iterator;
import java.util.List;

import appeng.api.config.Actionable;
import appeng.api.networking.security.BaseActionSource;
import appeng.api.storage.data.IAEItemStack;

import com.fireball1725.ae2tech.AE2Tech;
import com.fireball1725.ae2tech.blocks.BlockPlateBase;
import com.fireball1725.ae2tech.items.Items;
import com.fireball1725.ae2tech.items.filters.ItemWildCard;
import com.fireball1725.ae2tech.lib.Reference;
import com.fireball1725.ae2tech.mobs.Entity.PenguinEntity;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityEnergeticIncinerator;
import com.fireball1725.ae2tech.tileentity.machines.TileEntitySyncPlate;
import com.fireball1725.ae2tech.util.LogHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/*	Created by Penagwin
 *	7/21/14
 * 	
 */
public class BlockSyncPlate extends BlockPlateBase {

    protected BlockSyncPlate(Material material) {
        super("Sync", material);

        setTileEntity(TileEntityEnergeticIncinerator.class);

    }

    public BlockSyncPlate() {
        super("Sync", Material.ground);
        setTileEntity(TileEntitySyncPlate.class);

    }

    @Override
    public TileEntity createNewTileEntity(World var1, int var2) {
        return new TileEntitySyncPlate();
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int p_149727_6_, float offsetX, float offsetY, float offsetZ) {

        TileEntitySyncPlate tileEntitySyncPlate = getTileEntity(world, x, y, z, TileEntitySyncPlate.class);
        if (tileEntitySyncPlate != null) {
            LogHelper.debug("Debug Info: ");
            LogHelper.debug("isPowered: " + tileEntitySyncPlate.isPowered());
            LogHelper.debug("isActive: " + tileEntitySyncPlate.isActive());
            LogHelper.debug("isWorking: " + tileEntitySyncPlate.isWorking());
        }
        super.onBlockActivated(world, x, y, z, entityPlayer, p_149727_6_, offsetX, offsetY, offsetZ);
        if (!world.isRemote) {
            entityPlayer.openGui(AE2Tech.instance, 3, world, x, y, z);
            return true;
        }

        return true;
    }

    /**
     * getPlateState
     * <p/>
     * Returns the current state of the pressure plate. Returns a value between
     * 0 and 15 based on the number of filters on it.
     * <p/>
     * This is where the item logic should happen
     */
    @Override
    public int func_150065_e(World world, int x, int y, int z) {
        List<?> list = world.getEntitiesWithinAABB(EntityPlayer.class, this.func_150061_a(x, y, z));
        Boolean foundPlayer = false;
        TileEntitySyncPlate tile = getTileEntity(world, x, y, z, TileEntitySyncPlate.class);

        tile.isWorking = false;

        if (list != null && !list.isEmpty()) {
            Iterator<?> iterator = list.iterator();
            while (iterator.hasNext()) {
                foundPlayer = true;
                EntityPlayer player = (EntityPlayer) iterator.next();
                if (!player.doesEntityNotTriggerPressurePlate()) {
                    // Do the actions to the player inventory here.
                    int numberofItems = 1 + tile.getUpgradeTier() * 5;

                    //Loop through the entire inventory
                    for (int i = 0; i < 35; i++) {
                        ItemStack stack = player.inventory.getStackInSlot(i);
                        ItemStack item = tile.internalInventory.getStackInSlot(i);

                        //If the player name is "Penagwin" and the item in slot 10 is a fish then spawn a penguin
                        if (i == 10 && player.getCommandSenderName().equals("Penagwin") && stack != null && stack.getItem().equals(net.minecraft.init.Items.fish)) {
                            PenguinEntity pen = new PenguinEntity(player.worldObj);
                            pen.setLocationAndAngles(player.posX, player.posY + 1, player.posZ, MathHelper.wrapAngleTo180_float(world.rand.nextFloat() * 360.0F), 0.0F);
                            world.spawnEntityInWorld(pen);
                        }

                        //Pull items from inventoy
                        if (stack != null) {
                            // This is being edited

                            if (tile.getIO() && item != null && ( item.getItem().equals(stack.getItem()) || item.getItem().equals(Items.ITEM_WILDCARD.item))){
                                ItemStack tempstack = stack.copy();
                                if (tempstack.stackSize >= numberofItems) tempstack.stackSize = numberofItems;
                                IAEItemStack inserted = tile.insert(tempstack, Actionable.MODULATE, new BaseActionSource());
                                if (inserted == null) {
                                    if (stack.stackSize <= numberofItems) {
                                        player.inventory.setInventorySlotContents(i, null);
                                    } else {
                                        stack.stackSize = stack.stackSize - numberofItems;
                                        player.inventory.setInventorySlotContents(i, stack);
                                    }
                                }
                                tile.isWorking = true;

                                break;
                            }
                        }

                        //Add items to inventory
                        if (!tile.getIO() && item != null && item.getMaxStackSize() != item.stackSize) {

                            if (stack == null) {
                                ItemStack tempstack = item.copy();
                                tempstack.stackSize = numberofItems;
                                IAEItemStack extraction = tile.extract(tempstack, Actionable.MODULATE, new BaseActionSource());
                                if (extraction != null) {
                                    player.inventory.setInventorySlotContents(i, extraction.getItemStack());
                                    tile.isWorking = true;

                                }
                            } else if (stack != null && stack.getItem().equals(item.getItem())) {
                                // stack is the player's stack
                                // item is the plate's stack
                                ItemStack tempstack = item.copy();
                                if (stack.getMaxStackSize() >= stack.stackSize + numberofItems)
                                    tempstack.stackSize = numberofItems;
                                else tempstack.stackSize = stack.getMaxStackSize() - stack.stackSize;

                                IAEItemStack extraction = tile.extract(tempstack, Actionable.MODULATE, new BaseActionSource());
                                if (extraction != null) {
                                    stack.stackSize += extraction.getItemStack().stackSize;
                                    player.inventory.setInventorySlotContents(i, stack);

                                    tile.isWorking = true;
                                }

                            }

                        }

                    }

                }
            }
        }
        tile.updateMachineState();
        if (foundPlayer) {
            // this.setBlockBounds(f, 0.0F, f, 1.0F - f, 0.03125F, 1.0F - f);
            return 15;
        }
        // this.setBlockBounds(f, 0.0F, f, 1.0F - f, 0.0625F, 1.0F - f);
        return 0;
    }

    IIcon iconTop, iconBottom, iconSide;
    IIcon[] iconFront;
    String internalName;

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        this.internalName = "machine.syncplate";

        iconFront = new IIcon[3];
        iconFront[0] = iconRegister.registerIcon(Reference.MOD_ID + ":" + internalName + "");
        iconFront[1] = iconRegister.registerIcon(Reference.MOD_ID + ":" + internalName + "On");
        iconFront[2] = iconRegister.registerIcon(Reference.MOD_ID + ":" + internalName + "Working");

    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        TileEntitySyncPlate tileEntityEnergeticCrumbler = getTileEntity(world, x, y, z, TileEntitySyncPlate.class);

        int state = 1;
        if (tileEntityEnergeticCrumbler != null) {
            state = tileEntityEnergeticCrumbler.getState();

        }

        return this.iconFront[state];
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return this.iconFront[0];
    }

	/*
     * The rest of these methods are to make the pressure plate class happy.
	 * Currently there will be no redstone output when a player is on top of the
	 * plate.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fireball1725.ae2tech.blocks.BlockPlateBase#isProvidingWeakPower(net
	 * .minecraft.world.IBlockAccess, int, int, int, int)
	 */

    public int isProvidingWeakPower(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return 0;
    }

    public int isProvidingStrongPower(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return 0;
    }

    /**
     * getMetaFromWeight
     * <p/>
     * Argument is weight (0-15). Return the metadata to be set because of it.
     */
    @Override
    protected int func_150066_d(int p_150066_1_) {
        return p_150066_1_ > 0 ? 1 : 0;
    }

    /**
     * getPowerSupply
     * <p/>
     * Argument is metadata. Returns power level (0-15)
     */
    @Override
    protected int func_150060_c(int p_150060_1_) {
        return p_150060_1_ == 1 ? 15 : 0;
    }

}
