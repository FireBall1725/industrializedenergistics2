package com.fireball1725.ae2tech;

import appeng.api.AEApi;
import appeng.api.config.Upgrades;

import com.fireball1725.ae2tech.core.sync.Handler;
import com.fireball1725.ae2tech.core.sync.network.NetworkHandler;
import com.fireball1725.ae2tech.handler.ConfigurationHandler;
import com.fireball1725.ae2tech.lib.Autoupdate;
import com.fireball1725.ae2tech.lib.Reference;
import com.fireball1725.ae2tech.proxy.IProxy;
import com.fireball1725.ae2tech.reference.Settings;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityDeforester;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityLiquifier;
import com.fireball1725.ae2tech.tileentity.machines.TileEntitySyncPlate;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import java.io.File;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, dependencies = Reference.DEPENDENCIES, version = Reference.VERSION_BUILD)
public class AE2Tech {
    @Mod.Instance
    public static AE2Tech instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    public static IProxy proxy;

    public static String LogDebugString = "@DEBUG@";
    public static boolean LogDebug = true;
    private static File configFolder;

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
        // create our mod's channel.
        NetworkRegistry.INSTANCE.newChannel("guiButton", new Handler());
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ConfigurationHandler.init(event.getSuggestedConfigurationFile());

        //if(!LogDebugString.equals("@DEBUG@")) {
        //	LogDebug = false;
        //}
        if (Settings.SETTINGS_DEBUG_FORCEDEBUG) {
            LogDebug = true;
        }

        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GUIHandler());

        // Register Blocks, TileEntities, and Items
        proxy.registerBlocks();
        proxy.registerItems();

        // Regiser Events
        proxy.registerEvents();

        // Register AE things
        proxy.registerAECardIntergration();
        proxy.registerCrumblerRecipes();
        GameRegistry.registerTileEntity(TileEntitySyncPlate.class, "tile." + Reference.MOD_ID + ".syncplate");
       // GameRegistry.registerTileEntity(TileEntityDeforester.class, "tile." + Reference.MOD_ID + ".deforester");

        //GameRegistry.registerTileEntity(TileEntityLiquifier.class, "tile." + Reference.MOD_ID + ".liquifier");

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.registerRecipes(configFolder);
        proxy.registerRenderer();

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        NetworkHandler.instance = new NetworkHandler(Reference.MOD_ID);

        if (Settings.SETTINGS_UPDATES_DISABLECHECK) {
            if (Autoupdate.CheckForUpdates()) {
                FMLInterModComms.sendRuntimeMessage(this, "VersionChecker", "addUpdate", Autoupdate.GetUpdateDetails());
            }
        }
    }
}
