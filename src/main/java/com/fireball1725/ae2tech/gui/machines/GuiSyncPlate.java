package com.fireball1725.ae2tech.gui.machines;

import io.netty.buffer.ByteBuf;

import com.fireball1725.ae2tech.container.machines.ContainerSyncPlate;
import com.fireball1725.ae2tech.core.localization.GuiText;

import com.fireball1725.ae2tech.gui.BaseGui;
import com.fireball1725.ae2tech.gui.widgets.GuiProgressBar;
import com.fireball1725.ae2tech.tileentity.machines.TileEntitySyncPlate;

import cpw.mods.fml.common.network.ByteBufUtils;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiButton;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.network.play.client.C17PacketCustomPayload;
import static io.netty.buffer.Unpooled.buffer;

/*	Created by Penagwin
 *	7/22/14
 * 	
 */
public class GuiSyncPlate extends BaseGui {
	GuiProgressBar guiProgressBar;
	ContainerSyncPlate containerSyncPlate;
	InventoryPlayer inventoryPlayer;

	public GuiSyncPlate(InventoryPlayer inventoryPlayer, TileEntitySyncPlate tileEntityEnergeticIncinerator) {
		super(new ContainerSyncPlate(inventoryPlayer, tileEntityEnergeticIncinerator));
		containerSyncPlate = ((ContainerSyncPlate) this.inventorySlots);
		this.xSize = 211;
		this.ySize = 233;
		this.inventoryPlayer = inventoryPlayer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initGui() {
		super.initGui();

		if (containerSyncPlate.tileEntitySyncPlate.allInv) {
			//this.buttonList.add(new GuiButton(2, this.guiLeft + 70, this.guiTop + 5, 100, 20,  "Whole Inventory"));
		} else {
			//this.buttonList.add(new GuiButton(2, this.guiLeft + 70, this.guiTop + 5, 100, 20, "Leave Hotbar"));
		}
		if (containerSyncPlate.getSyncIO()) {
			this.buttonList.add(new GuiButton(1, this.guiLeft + 70, this.guiTop + 5, 100, 20, "Pull"));
		} else {
			this.buttonList.add(new GuiButton(1, this.guiLeft + 70, this.guiTop + 5, 100, 20, "Push"));
		}
	}

	protected void actionPerformed(GuiButton guibutton) {
		// id is the id you give your button
		switch (guibutton.id) {
		case 1:
			containerSyncPlate.tileEntitySyncPlate.toggleIO();
			//System.out.println("SENDING");
			ByteBuf data = buffer(10);
			TileEntitySyncPlate tile = containerSyncPlate.tileEntitySyncPlate;
			data.writeInt(1);

			data.writeInt(tile.xCoord);
			data.writeInt(tile.yCoord);
			data.writeInt(tile.zCoord);

			EntityClientPlayerMP player = (EntityClientPlayerMP) inventoryPlayer.player;

			ByteBufUtils.writeUTF8String(data, player.getDisplayName());
			C17PacketCustomPayload packet = new C17PacketCustomPayload("guiButton", data);
			player.sendQueue.addToSendQueue(packet);
			
			if (containerSyncPlate.getSyncIO()) {
				guibutton.displayString = "Pull";

			} else {
				guibutton.displayString = "Push";

			}
			break;
		case 2:
			containerSyncPlate.tileEntitySyncPlate.toggleAllInv();

			//System.out.println("SENDING");
			ByteBuf data1 = buffer(10);
			TileEntitySyncPlate tile1 = containerSyncPlate.tileEntitySyncPlate;
			data1.writeInt(2);

			data1.writeInt(tile1.xCoord);
			data1.writeInt(tile1.yCoord);
			data1.writeInt(tile1.zCoord);

			EntityClientPlayerMP player1 = (EntityClientPlayerMP) inventoryPlayer.player;

			ByteBufUtils.writeUTF8String(data1, player1.getDisplayName());
			C17PacketCustomPayload packet1 = new C17PacketCustomPayload("guiButton", data1);
			player1.sendQueue.addToSendQueue(packet1);
			
			if (containerSyncPlate.tileEntitySyncPlate.allInv) {
				guibutton.displayString = "Whole Inventory";

			} else {
				guibutton.displayString = "Leave Hotbar";

			}
			break;
		case 3:
			break;
		}

	}

	public void drawBG(int offsetX, int offsetY, int mouseX, int mouseY) {
		bindTexture("gui/syncplate.png");
		drawTexturedModalRect(offsetX, offsetY, 0, 0, this.xSize, this.ySize);
	}

	public void drawFG(int offsetX, int offsetY, int mouseX, int mouseY) {
		double progress = (this.containerSyncPlate.progress + 1);
		int maxFurnaceCookTime = 150 - ((this.containerSyncPlate.upgradeTier + 1) * 25);

		progress = (progress / maxFurnaceCookTime);
		progress = progress * 100;


		this.fontRendererObj.drawString(getGuiDisplayName(GuiText.SyncPlate.getLocal()), 8, 5 , 4210752);
		this.fontRendererObj.drawString(GuiText.inventory.getLocal(), 8, 110, 4210752);
	}
}
