package com.fireball1725.ae2tech.gui.machines;

import appeng.api.config.RedstoneMode;
import appeng.api.config.Settings;
import com.fireball1725.ae2tech.container.machines.ContainerDeforester;
import com.fireball1725.ae2tech.core.localization.GuiText;
import com.fireball1725.ae2tech.gui.BaseGui;
import com.fireball1725.ae2tech.gui.widgets.GuiImageButton;
import com.fireball1725.ae2tech.gui.widgets.GuiProgressBar;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityDeforester;
import net.minecraft.entity.player.InventoryPlayer;

public class GuiDeforester extends BaseGui {
    GuiProgressBar guiProgressBar;
    ContainerDeforester containerDeforester;
    GuiImageButton redstoneMode;

    public GuiDeforester(InventoryPlayer inventoryPlayer, TileEntityDeforester tileEntityDeforester) {
        super(new ContainerDeforester(inventoryPlayer, tileEntityDeforester));
        containerDeforester = ((ContainerDeforester) this.inventorySlots);
        this.xSize = 211;
        this.ySize = 152;
    }

    @Override
    public void initGui() {
        super.initGui();
        guiProgressBar = new GuiProgressBar("gui/energeticincinerator.png", this.guiLeft + 156, this.guiTop + 27, 214, 27, 6, 18, GuiProgressBar.Direction.VERTICAL);
        this.buttonList.add(this.guiProgressBar);
        addButtons();
    }

    protected void addButtons() {
        this.redstoneMode = new GuiImageButton(this.guiLeft - 18, this.guiTop, Settings.REDSTONE_EMITTER, RedstoneMode.LOW_SIGNAL);
        this.buttonList.add(this.redstoneMode);
    }

    public void drawBG(int offsetX, int offsetY, int mouseX, int mouseY) {
        bindTexture("gui/energeticincinerator.png");
        drawTexturedModalRect(offsetX, offsetY, 0, 0, this.xSize, this.ySize);
    }

    public void drawFG(int offsetX, int offsetY, int mouseX, int mouseY) {
        double progress = (this.containerDeforester.progress + 1);
        int maxFurnaceCookTime = 150 - ((this.containerDeforester.upgradeTier + 1) * 25);

        progress = (progress / maxFurnaceCookTime);
        progress = progress * 100;
        this.guiProgressBar.current = (int) progress;

        this.fontRendererObj.drawString(getGuiDisplayName(GuiText.EnergeticIncinerator.getLocal()), 8, 8, 4210752);
        this.fontRendererObj.drawString(GuiText.inventory.getLocal(), 8, this.ySize - 96 + 2, 4210752);
    }
}
