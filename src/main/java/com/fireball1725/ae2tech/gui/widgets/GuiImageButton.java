package com.fireball1725.ae2tech.gui.widgets;

import appeng.api.config.OperationMode;
import appeng.api.config.RedstoneMode;
import appeng.api.config.Settings;
import com.fireball1725.ae2tech.helpers.ButtonToolTips;
import com.fireball1725.ae2tech.lib.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;

import java.util.HashMap;
import java.util.Map;

public class GuiImageButton extends GuiButton implements ITooltip {
    class BtnAppearance {
        public int index;
        public String DisplayName;
        public String DisplayValue;

        BtnAppearance() {}
    }

    class EnumPair {
        Enum setting;
        Enum value;

        EnumPair(Enum a, Enum b) {
            this.setting = a;
            this.value = b;
        }

        @Override
        public int hashCode() {
            return this.setting.hashCode() ^ this.value.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            EnumPair d = (EnumPair)obj;
            return (d.setting.equals(this.setting)) && (d.value.equals(this.value));
        }
    }

    public boolean halfSize = false;
    public String FillVar;
    private final Enum buttonSetting;
    private Enum currentValue;
    private static Map<EnumPair, BtnAppearance> Appearances;

    private void registerApp(int IIcon, Settings setting, Enum val, ButtonToolTips title, Object hint) {
        BtnAppearance a = new BtnAppearance();
        a.DisplayName = title.getUnlocalized();
        a.DisplayValue = ((String)((hint instanceof String) ? hint : ((ButtonToolTips)hint).getUnlocalized()));
        a.index = IIcon;
        Appearances.put(new EnumPair(setting, val), a);
    }

    public void setVisibility(boolean vis) {
        this.visible = vis;
        this.enabled = vis;
    }

    public GuiImageButton(int x, int y, Enum idx, Enum val) {
        super (0, 0, 16, "");
        this.buttonSetting = idx;
        this.currentValue = val;
        this.xPosition = x;
        this.yPosition = y;
        this.width = 16;
        this.height = 16;
        if (Appearances == null) {
            Appearances = new HashMap();

            registerApp(3, Settings.REDSTONE_CONTROLLED, RedstoneMode.IGNORE, ButtonToolTips.RedstoneMode, ButtonToolTips.AlwaysActive);
            registerApp(0, Settings.REDSTONE_CONTROLLED, RedstoneMode.LOW_SIGNAL, ButtonToolTips.RedstoneMode, ButtonToolTips.ActiveWithoutSignal);
            registerApp(1, Settings.REDSTONE_CONTROLLED, RedstoneMode.HIGH_SIGNAL, ButtonToolTips.RedstoneMode, ButtonToolTips.ActiveWithSignal);
            registerApp(2, Settings.REDSTONE_CONTROLLED, RedstoneMode.SIGNAL_PULSE, ButtonToolTips.RedstoneMode, ButtonToolTips.ActiveOnPulse);

            //registerApp(51, Settings.OPERATION_MODE, OperationMode.FILL, ButtonToolTips.TransferDirection, ButtonToolTips.TransferFromNetwork);
            //registerApp(50, Settings.OPERATION_MODE, OperationMode.EMPTY, ButtonToolTips.TransferDirection, ButtonToolTips.TransferToNetwork);
        }
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void drawButton(Minecraft par1Minecraft, int par2, int par3) {
        if (this.visible) {
            int iconIndex = getIconIndex();
            if (this.halfSize) {
                this.width = 8;
                this.height = 8;

                GL11.glPushMatrix();
                GL11.glTranslatef(this.xPosition, this.yPosition, 0.0F);
                GL11.glScalef(0.5F, 0.5F, 0.5F);

                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                //par1Minecraft.renderEngine.bindTexture(ExtraTextures.GuiTexture("guis/states.png"));
                ResourceLocation resourceLocation = new ResourceLocation("appliedenergistics2", "textures/" + "guis/states.png");
                par1Minecraft.getTextureManager().bindTexture(resourceLocation);
                this.field_146123_n = ((par2 >= this.xPosition) && (par3 >= this.yPosition) && (par2 < this.xPosition + this.width) && (par3 < this.yPosition + this.height));

                int uv_y = (int)Math.floor(iconIndex / 16);
                int uv_x = iconIndex - uv_y * 16;

                drawTexturedModalRect(0, 0, 240, 240, 16, 16);
                drawTexturedModalRect(0, 0, uv_x * 16, uv_y * 16, 16, 16);
                mouseDragged(par1Minecraft, par2, par3);

                GL11.glPopMatrix();
            } else {
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                //par1Minecraft.renderEngine.bindTexture(ExtraTextures.GuiTexture("guis/states.png"));
                ResourceLocation resourceLocation = new ResourceLocation("appliedenergistics2", "textures/" + "guis/states.png");
                par1Minecraft.getTextureManager().bindTexture(resourceLocation);

                this.field_146123_n = ((par2 >= this.xPosition) && (par3 >= this.yPosition) && (par2 < this.xPosition + this.width) && (par3 < this.yPosition + this.height));

                int uv_y = (int)Math.floor(iconIndex / 16);
                int uv_x = iconIndex - uv_y * 16;

                drawTexturedModalRect(this.xPosition, this.yPosition, 240, 240, 16, 16);
                drawTexturedModalRect(this.xPosition, this.yPosition, uv_x * 16, uv_y * 16, 16, 16);
                mouseDragged(par1Minecraft, par2, par3);
            }
        }
    }

    private int getIconIndex() {
        if ((this.buttonSetting != null) && (this.currentValue != null)) {
            BtnAppearance app = (BtnAppearance)Appearances.get(new EnumPair(this.buttonSetting, this.currentValue));
            if (app == null) {
                return 255;
            }
            return app.index;
        }
        return 255;
    }

    public Settings getSettings() {
        return (Settings)this.buttonSetting;
    }

    public Enum getCurrentValue() {
        return this.currentValue;
    }

    @Override
    public String getMessage() {
        String DisplayName = null;
        String DisplayValue = null;
        if ((this.buttonSetting != null) && (this.currentValue != null)) {
            BtnAppearance ba = (BtnAppearance)Appearances.get(new EnumPair(this.buttonSetting, this.currentValue));
            if (ba == null) {
                return "No Message...";
            }
            DisplayName = ba.DisplayName;
            DisplayValue = ba.DisplayValue;
        }

        if (DisplayName != null) {
            String Name = StatCollector.translateToLocal(DisplayName);
            String Value = StatCollector.translateToLocal(DisplayValue);

            if ((Name == null) || (Name.equals(""))) {
                Name = DisplayName;
            }

            if ((Value == null) || (Value.equals(""))) {
                Value = DisplayValue;
            }

            if (this.FillVar != null) {
                Value = Value.replaceFirst("%s", this.FillVar);
            }

            Value = Value.replace("\\n", "\n");
            StringBuilder sb = new StringBuilder(Value);

            int i = sb.lastIndexOf("\n");
            if (i <= 0) {
                i = 0;
            }

            while ((i + 30 < sb.length()) && ((i = sb.lastIndexOf(" ", i + 30)) != -1)) {
                sb.replace(i, i + 1, "\n");
            }

            return Name + "\n" + sb.toString();
        }
        return null;
    }

    @Override
    public int xPos() {
        return this.xPosition;
    }

    @Override
    public int yPos() {
        return this.yPosition;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    public void set(Enum e) {
        if (this.currentValue != e) {
            this.currentValue = e;
        }
    }
}
