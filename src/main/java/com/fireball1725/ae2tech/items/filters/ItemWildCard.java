package com.fireball1725.ae2tech.items.filters;

/**
 * Created by penagwin on 11/26/14.
 */

import com.fireball1725.ae2tech.lib.Reference;
import net.minecraft.item.Item;



public class ItemWildCard extends Item{


    public ItemWildCard() {
        super();
        setTextureName(Reference.MOD_ID + ":filter.wildcard.png");
    }

}
