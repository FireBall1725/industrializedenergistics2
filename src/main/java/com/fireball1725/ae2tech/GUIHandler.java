package com.fireball1725.ae2tech;

import com.fireball1725.ae2tech.container.machines.*;
import com.fireball1725.ae2tech.gui.machines.*;
import com.fireball1725.ae2tech.tileentity.machines.*;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GUIHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(x, y, z);
        if (tileEntity != null) {
            switch (ID) {
                case 0: // Energetic Incinerator GUI
                    return new ContainerEnergeticIncinerator(player.inventory, (TileEntityEnergeticIncinerator) tileEntity);
                case 1: // Advanced Energetic Incinerator GUI
                    return new ContainerAdvancedEnergeticIncinerator(player.inventory, (TileEntityAdvancedEnergeticIncinerator) tileEntity);
                case 2: // Energetic Crumbler GUI
                    return new ContainerEnergeticCrumbler(player.inventory, (TileEntityEnergeticCrumbler) tileEntity);
                case 3:
                    return new ContainerSyncPlate(player.inventory, (TileEntitySyncPlate) tileEntity);
                case 4: // Energetic Incinerator GUI
                    return new ContainerLiquifier(player.inventory, (TileEntityLiquifier) tileEntity);
                case 5: // Energetic Incinerator GUI
                    return new ContainerDeforester(player.inventory, (TileEntityDeforester) tileEntity);

                default:
                    return false;
            }
        }

        return false;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(x, y, z);
        if (tileEntity != null) {
            switch (ID) {
                case 0: // Energetic Incinerator GUI
                    return new GuiEnergeticIncinerator(player.inventory, (TileEntityEnergeticIncinerator) tileEntity);
                case 1: // Advanced Energetic Incinerator GUI
                    return new GuiAdvancedEnergeticIncinerator(player.inventory, (TileEntityAdvancedEnergeticIncinerator) tileEntity);
                case 2: // Energetic Crumbler GUI
                    return new GuiEnergeticCrumbler(player.inventory, (TileEntityEnergeticCrumbler) tileEntity);
                case 3:
                    return new GuiSyncPlate(player.inventory, (TileEntitySyncPlate) tileEntity);
                case 4:
                    return new GuiLiquifier(player.inventory, (TileEntityLiquifier) tileEntity);
                case 5:
                    return new GuiDeforester(player.inventory, (TileEntityDeforester) tileEntity);
                default:
                    return false;
            }
        }

        return false;
    }
}
